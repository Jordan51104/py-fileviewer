import sys
import os

x = 0

if (sys.argv[1] == "--help"):
	print("""
Fileviewer
Made with Python 3.7

Command Line Arguments:
	--help: display this help text
	--file-type: set the file type to look for
	--dir: directory to look through
	""")

while (x < len(sys.argv)):
	arg = sys.argv[x]
	if (arg == "--file-type"):
		file_type = sys.argv[x+1]
	if (arg == "--dir"):
		directory = sys.argv[x+1]
	x += 1
