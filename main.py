import sys
import os

if (len(sys.argv) == 1):
	print("You didn't supply enough arguments!")
	sys.exit()

if (sys.argv[1] == "--help" or sys.argv[1] == "-h"):
	print("""Fileviewer
Made with Python 3.7

Command Line Arguments:
	--help: display this help text
	--file-type: set the file type to look for
	--dir: directory to look through
	""")
	sys.exit()
x = 0
directory = ""
 
while (x < len(sys.argv)):
	arg = sys.argv[x]
	if (arg == "--file-type"):
		global file_type
		file_type = sys.argv[x+1]
	if (arg == "--dir"):
		if (sys.argv[x+1] == "."):
			directory = os.popen("pwd")
		else:
			directory = sys.argv[x+1]
		directory = sys.argv[x+1]
	x += 1

files = os.popen("ls " + dir + " | grep -E '[a-zA-Z]{1,}.pdf'").read()
files_split = files.split("\n")
files_popped = files_split.pop()

x = 1

for file in files_split:
	print(str(x) + " " + file)
	x += 1

file_view = input("\nWhat file do you want to view?\n")

selected_file = files_split[int(file_view) - 1]

os.popen("zathura " + directory + selected_file + " &")
